const directions = {
    UP: 'up',
    LEFT: 'left',
    RIGHT: 'right',
    DOWN: 'down',
};

const colValues = {
    WALL: 't',
    OPEN: 'f',
    PATH: 'fp',
    DEADEND: 'fx',
};

//Set up the start and exits square
let startRow = 0;
let startCol = 0;
let exitRow = 3;
let exitCol = 0;

// with a known start and exit get to the exit with the least amount of steps
function solveMaze() {
    let grid = cGrid;
    let curRow = 0;
    let curCol = 0;
    let prevRow = 0;
    let prevCol = 0;
    let stepCount = 0;
    let exitReached = false;
    let noExit = false;
    let minDistance = -1;
    let nextDirection;
    let moveUp = {};
    let moveLeft = {};
    let moveRight = {};
    let moveDown = {};


    //Mark the start as part of the path
    grid[startRow][startCol] = colValues.PATH;
    let elementID = `${startRow}:${startCol}`;
    document.getElementById(elementID).setAttribute('blockValue', 'step');

    // solve the maze by looking for the next best step
    do {
        let nextStep = [];
        let prevRow = curRow;
        let prevCol = curCol;

        moveUp = move(curRow, curCol, grid, directions.UP);
        if (moveUp.canMove === true) {
            nextStep.push(moveUp);
        }

        moveDown = move(curRow, curCol, grid, directions.DOWN);
        if (moveDown.canMove === true) {
            nextStep.push(moveDown);
        }

        moveLeft = move(curRow, curCol, grid, directions.LEFT);
        if (moveLeft.canMove === true) {
            nextStep.push(moveLeft);
        }

        moveRight = move(curRow, curCol, grid, directions.RIGHT);
        if (moveRight.canMove === true) {
            nextStep.push(moveRight);
        }

        // if we have nowhere to go exit
        if (nextStep.length === 0) {
            noExit = true;
            break;
        }

        // sort next step by min distance
        nextStep.sort((a, b) => (a.minDistance - b.minDistance));

        // pick the element that is closest to the exit. Pick Up or Down first.
        switch (nextStep[0].direction) {
            case directions.UP:
                // move up and add to step count
                stepCount++;
                curRow = curRow + 1;
                break;
            case directions.DOWN:
                stepCount++;
                curRow = curRow - 1;
                break;
            case directions.LEFT:
                stepCount++;
                curCol = curCol - 1;
                break;
            case directions.RIGHT:
                stepCount++;
                curCol = curCol + 1;
                break;
        }

        exitReached = markElements(curRow, curCol, prevRow, prevCol, grid);
    }
    while(exitReached === false || noExit === true);
    if (exitReached === true) {
        document.getElementById('results').innerHTML = `Success! It took ${stepCount} step(s)`;
    } else {
        document.getElementById('results').innerHTML = 'Cannot Find an Exit';
    }
}

// solve the zero hour mazes
function solveMazeZH() {
    let grid = cGrid;
    let curRow = 0;
    let curCol = 0;
    let prevRow = 0;
    let prevCol = 0;
    let stepCount = 0;
    let exitReached = false;
    let noExit = false;
    let minDistance = -1;
    let nextDirection;
    let moveUp = {};
    let moveLeft = {};
    let moveRight = {};
    let moveDown = {};

    // find the start column - it's om row 0

    exitRow = grid.length - 1;
    startRow = 0;
    startCol = grid[startRow].findIndex(isPath);

    curCol = startCol;
    curRow = startRow;

    // mark the start as part of the path
    grid[startRow][startCol] = colValues.PATH;
    let elementID = `${startRow}:${startCol}`;
    document.getElementById(elementID).setAttribute('blockValue', 'step');

    // solve the maze by looking for the next best step

    do {
        let nextStep = [];
        let prevRow = curRow;
        let prevCol = curCol;

        moveUp = moveZH(curRow, curCol, grid, directions.UP);
        if (moveUp.canMove === true) {
            nextStep.push((moveUp));
        }
        moveDown = moveZH(curRow, curCol, grid, directions.DOWN);
        if (moveDown.canMove === true) {
            nextStep.push(moveDown);
        }

        moveLeft = moveZH(curRow, curCol, grid, directions.LEFT);
        if (moveLeft.canMove === true) {
            nextStep.push(moveLeft);
        }

        moveRight = moveZH(curRow, curCol, grid, directions.RIGHT);
        if (moveRight.canMove === true) {
            nextStep.push(moveRight);
        }

        // if we have nowhere to go exit
        if (nextStep.length === 0) {
            noExit = true;
            break;
        }

        // sort next step by target value
        nextStep.sort(function (a, b) {
            if (a.colValue > b.colValue) {
                return 1;
            }
            if (a.colValue < b.colValue) {
                return -1;
            }
            return 0;
        });

        // pick the element that is closes to the exit
        switch (nextStep[0].direction) {
            case directions.UP:
                //move up and add to step count
                stepCount++;
                curRow = curRow + 1;
                break;
            case directions.DOWN:
                //Move Down and add to step count
                stepCount++;
                curRow = curRow - 1;
                break;
            case directions.LEFT:
                //Move left and add to step count
                stepCount++;
                curCol = curCol - 1;
                break;
            case directions.RIGHT:
                //Move right and add to step count
                stepCount++;
                curCol = curCol + 1;
                break;

        }

        // mark the squares on the page
        if (curRow === exitRow) {
            exitCol = curCol;
        }

        exitReached = markElements(curRow, curCol, prevRow, prevCol, grid);
    } while (exitReached === false || noExit === true);
    if (exitReached === true) {
        document.getElementById('results').innerHTML = `Success! It took ${stepCount} step(s)`
    }
}


// see if we can move to the next square and calculate the distance to the exit
function move(curRow, curCol, grid, direction) {
    let targetRow = curRow;
    let targetCol = curCol;
    let targetVal = '';
    let canMove = false;
    let minDistance = -1;

    switch (direction) {
        case directions.UP:
            targetRow = curRow + 1;
            break;
        case directions.LEFT:
            targetCol = curCol - 1;
            break;
        case directions.RIGHT:
            targetCol = curCol + 1;
            break;
        case directions.DOWN:
            targetRow = curRow - 1;
            break;
    }

    // check for out of bounds
    if (targetRow > grid.length - 1 || targetRow < 0 || targetCol > grid[targetRow].length || targetCol < 0) {
        return {
            canMove: false,
            minDistance: -1,
            direction: direction,
            colValue: colValues.WALL,
        };
    }

    // get the value of the square we are trying to move to
    targetVal = grid[targetRow][targetCol];


    if (targetRow === startRow && targetCol === startCol) {
        // we cannot move back to the start
        return {
            canMove: false,
            minDistance: -1,
            direction: direction,
            colValue: colValues.WALL,
        };
    } else if (targetVal === colValues.OPEN) {
        //test if we can move to the target square.'f' means no wall
        //calculate the distance to the exit
        return {
            canMove: true,
            minDistance: GetMinDistance(targetRow, targetCol),
            direction: direction,
            colValue: targetVal
        };
    } else if (targetVal === colValues.WALL || targetVal === colValues.DEADEND) {
        // test for a wall or deadend: 't' means wall, 'fx' means deadend
        return {
            canMove: false,
            minDistance: -1,
            direction: direction,
            colValue: targetVal,
        };
    } else if (targetVal === colValues.PATH) {
        // if you have to go backwards to a previous marked square, we need to mark
        // the current square as a dead end('fx'). 'fp' means square has already been marked
        return {
            canMove: true,
            minDistance: GetMinDistance(targetRow, targetCol),
            direction: direction,
            colValue: targetVal,
        };

    }

    return {
        canMove: false,
        minDistance: -1,
        direction: direction,
        colValue: colValues.WALL,
    }
}

function moveZH(curRow, curCol, grid, direction) {
    let targetRow = curRow;
    let targetCol = curCol;
    let targetVal = "";
    let canMove = false;
    let minDistance = -1;

    switch (direction) {
        case directions.UP:
            targetRow = curRow + 1;
            break;
        case directions.LEFT:
            targetCol = curCol - 1;
            break;
        case directions.RIGHT:
            targetCol = curCol + 1;
            break;
        case directions.DOWN:
            targetRow = curRow - 1;
            break;
    }

    // check for out bounds
    if (targetRow > grid.length - 1 || targetRow < 0 || targetCol > grid[targetRow].length || targetCol < 0) {
        return {
            canMove: false,
            minDistance: -1,
            direction: direction,
            colValue: colValues.WALL
        };
    }

    // get the value of the square we are trying to move to
    targetVal = grid[targetRow][targetCol];

    if (targetRow === startRow && targetCol === startCol) {
        //we cannot move back to start.
        return {
            canMove: false,
            minDistance: -1,
            direction: direction,
            colValue: colValues.WALL
        };

    } else if (targetVal === colValues.OPEN) {
        //test if we can move to the target square.'f' means no wall
        //calculate the distance to the exit
        return {
            canMove: true,
            minDistance: -1,
            direction: direction,
            colValue: targetVal
        };
    } else if (targetVal === colValues.WALL || targetVal === colValues.DEADEND) {
        //test for a wall or deadend; 't' means wall, 'fx' means deadend
        return {
            canMove: false,
            minDistance: -1,
            direction: direction,
            colValue: targetVal
        };
    } else if (targetVal === colValues.PATH) {
        //if you have to go backwards to a previous marked square
        //we need to mark the current square as a dead end ('fx')
        //'fp' means square has already been marked
        return {
            canMove: true,
            minDistance: -1,
            direction: direction,
            colValue: targetVal
        };
    }
    return {
        canMove: false,
        minDistance: -1,
        direction: direction,
        colValue: colValues.WALL
    };
}


//gets the minDistance between target and exit
function GetMinDistance(targetRow, targetCol) {
    return Math.abs(exitRow - targetRow) + Math.abs((exitCol - targetCol));
}

function markElements(targetRow, targetCol, prevRow, prevCol, grid) {
    let elementID = '';

    if (grid[targetRow][targetCol] === colValues.PATH) {
        grid[prevRow][prevCol] = colValues.DEADEND;
        elementID = `${prevRow}:${prevCol}`;
        document.getElementById(elementID).setAttribute('blockValue', 'deadend');
    }

    elementID = `${targetRow}:${targetCol}`;
    document.getElementById(elementID).setAttribute('blockValue', 'step');
    grid[targetRow][targetCol] = colValues.PATH;


    // test for exit
    return targetRow === exitRow && targetCol === exitCol;
}

function  isPath(element) {
    return element === 'f';

}



